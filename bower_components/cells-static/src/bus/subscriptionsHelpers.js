import { pipe } from '../core/utils';

const getTimed = function(subscriptions) {
  return subscriptions.filter(subscription => subscription.times ? true : false);
};


const getUntimed = function(subscriptions) {
  return subscriptions.filter(subscription => subscription.times ? false : true);
};


const decreaseTimes = function(subscriptions) {
  return subscriptions.map(subscription => {
    return Object.assign(subscription, { times: subscription.times - 1 });
  });
};


const unsubscribeZeroed = function(subscriptions) {
  return subscriptions.map(subscription => {
    if (subscription.times === 0) {
      delete subscription.times;
      subscription.unsubscribe();
    }
    return subscription;
  });
};


export const updateSubscriptions = function(subscriptions = []) {
  const untimed = getUntimed(subscriptions);
  const timed = pipe(
    getTimed,
    decreaseTimes,
    unsubscribeZeroed,
    getTimed
  )(subscriptions);

  return untimed.concat(timed);
};
