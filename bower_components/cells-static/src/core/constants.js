// pageRender
const mainNode = '#app__container';
const componentPath = 'app/pages';
const progressive = true;
const maxSimultaneousPages = 6;

// router
const useHash = true;
const routes = [];
const defaultRoute = null;
const hooks = {};

export const defaultPageRender = {
  mainNode,
  componentPath,
  progressive,
  maxSimultaneousPages
};

export const defaultRouter = {
  useHash,
  routes,
  defaultRoute,
  hooks
};

export default {
  defaultPageRender,
  defaultRouter
};
