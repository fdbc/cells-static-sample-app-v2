// constants
import CONSTANTS from './core/constants';
import CoreUtils from './core/utils';

// core dependencies
import Router from './router/index';
import Bus from './bus/index';

const { defaultPageRender, defaultRouter } = CONSTANTS;
const { extendObject } = CoreUtils;

export default class CellsStatic {
  Router = null;
  Bus = null;

  constructor() {
    this._initDependencies();
  }

  config(options) {
    const { pageRender, router } = this._buildConfig(options);

    this.Router.bootstrap(pageRender, router);
  }

  _buildConfig(options) {
    const pageRender = extendObject(defaultPageRender, options.pageRender);
    const router = extendObject(defaultRouter, options.router);

    return {
      pageRender,
      router
    };
  }

  _initDependencies(dependencies) {
    console.log('_initDependencies');

    dependencies = Object.assign(
      {
        Router,
        Bus
      },
      dependencies
    );

    for (let dependence in dependencies) {
      if (dependencies.hasOwnProperty(dependence)) {
        this[dependence] = new dependencies[dependence](this);
      }
    }
  }
}
