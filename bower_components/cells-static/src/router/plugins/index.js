import pageRender from './pageRender';
import progressivePageRender from './progressivePageRender';
import routerLifeCycle from './routerLifeCycle';

export default {
  pageRender,
  progressivePageRender,
  routerLifeCycle
};
