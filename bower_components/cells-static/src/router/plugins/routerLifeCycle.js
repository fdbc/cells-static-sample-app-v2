import { HOOKS } from '../constants';
import HooksUtils from '../lib/hooks';

const { ROUTER, ROUTE } = HOOKS;
const { getHookFromRouter, getHooksFromRouter, getHookFromRoute, getHooksFromRoutes, triggerHooks } = HooksUtils;

const routerLifeCycle = (routerLifeCycleHooks) => (router, dependencies) => {
  const { routes } = dependencies;

  return {
    onTransitionStart(toState, fromState) {
      console.log('routerLifeCycle::onTransitionStart()');

      const routerHooks = getHooksFromRouter([ROUTER.onTransitionStart], routerLifeCycleHooks);
      const routeHooks = getHooksFromRoutes([
        { hook: ROUTE.onStart, route: toState }
      ], routes);
      const hooks = [...routerHooks, ...routeHooks];

      triggerHooks(hooks, toState, fromState);
    },
    onTransitionCancel(toState, fromState) {
      console.log('routerLifeCycle::onTransitionCancel()');

      const routerHooks = getHooksFromRouter([ROUTER.onTransitionCancel], routerLifeCycleHooks);
      const routeHooks = getHooksFromRoutes([
        { hook: ROUTE.onCancel, route: toState }
      ], routes);
      const hooks = [...routerHooks, ...routeHooks];

      triggerHooks(hooks, toState, fromState);
    },
    onTransitionError(toState, fromState, err) {
      console.log('routerLifeCycle::onTransitionError()');

      const routerHooks = getHooksFromRouter([ROUTER.onTransitionError], routerLifeCycleHooks);
      const routeHooks = [];
      const hooks = [...routerHooks, ...routeHooks];

      triggerHooks(hooks, toState, fromState, err);
    },
    onTransitionSuccess(toState, fromState) {
      console.log('routerLifeCycle::onTransitionSuccess()');

      const routerHooks = getHooksFromRouter([ROUTER.onTransitionExit, ROUTER.onTransitionSuccess], routerLifeCycleHooks);
      const routeHooks = getHooksFromRoutes([
        { hook: ROUTE.onExit, route: fromState },
        { hook: ROUTE.onSuccess, route: toState }
      ], routes);
      const hooks = [...routerHooks, ...routeHooks];

      triggerHooks(hooks, toState, fromState);
    }
  }
};

export default routerLifeCycle;
