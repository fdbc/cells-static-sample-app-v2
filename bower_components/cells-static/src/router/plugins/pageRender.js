import CoreUtils from '../../core/utils';
import RouterUtils from '../utils';
import { querySelector } from '../lib/shadowQuerySelector';

const { pipe } = CoreUtils;
const { normalizePageName, getPageComponentName } = RouterUtils;

const normalizedPageComponentName = pipe(normalizePageName, getPageComponentName);

const pageRender = (mainNode) => () => {
  let appNode = null;

  return {
    onTransitionSuccess(toState, fromState) {
      const { name, params } = toState;
      const pageComponentName = normalizedPageComponentName(name);

      if (!appNode) {
        appNode = querySelector(mainNode, document.body);

        if (!appNode) {
          console.error(`${mainNode} node not found - ${pageComponentName} could not be rendered.`);
          return;
        }
      }

      const pageComponentNode = document.createElement(pageComponentName);
      const shouldBindParams = pageComponentNode.params && Object.keys(params).length > 0;

      if (shouldBindParams) {
        pageComponentNode.params= params;
      }

      /**
       * Remove all child elements of a DOM node in JavaScript
       * removeChild is the recommended way due to performance reasons, combining with firstChild.
       *
       * https://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript
       */
      while (appNode.firstChild) {
          appNode.removeChild(appNode.firstChild);
      }

      appNode.appendChild(pageComponentNode);
    }
  }
};

export default pageRender;
