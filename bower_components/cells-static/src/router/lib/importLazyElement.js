// taken from cells-bridge/src/manager/import
// adapted to new use case
export const importLazyElement = (endPoint) => {
  return new Promise((resolve, reject) => {
    const onload = () => {
      console.log('finished lazy loading of ' + endPoint);
      resolve();
    };
    const onerror = () => {
      console.log('failed lazy loading of ' + endPoint);
      reject();
    };

    const node = document.createElement('link');
    Object.assign(node, {
      rel: 'import',
      async: true,
      href: endPoint,
      onload,
      onerror
    });

    document.head.appendChild(node);

    console.log('starting lazy loading of ' + endPoint);
  });
};

export default {
  importLazyElement
};
