const isShadow = () => document.body.createShadowRoot || document.body.attachShadow;

const cleanSelectors = selectors => {
  return selectors.filter(function (sel) {
    return sel && sel.trim().length > 0;
  });
}

const collectionAsArray = collection => {
  var array = [];
  for (var i = 0; i < collection.length; i++) {
    array.push(collection.item(i));
  }

  return array;
}

const uniqueConcat = (original, added) => {
  var result = original.slice(0);
  added.forEach(function (element) {
    if (result.indexOf(element) < 0) {
      result.push(element);
    }
  });

  return result;
}

const getShadowChildren = element => {
  if (element.shadowRoot) {
    return collectionAsArray(element.shadowRoot.children);
  }

  return [];
}

const getCommonChildren = element => {
  return collectionAsArray(element.children);
}

const getChildren = element => {
  return [].concat(getShadowChildren(element)).concat(getCommonChildren(element));
}

const getNextElement = (currentElement, nextSelector) => {
  var children = getChildren(currentElement);
  var found = null;

  for (var i = 0; i < children.length; i++) {
    found = children[i].matches(nextSelector) ? children[i] : getNextElement(children[i], nextSelector);
    if (found) {
      break;
    }
  }

  return found;
}

const getNextElements = (currentElement, nextSelector) => {
  var children = getChildren(currentElement);
  var nextElements = [];

  children.forEach(function (child) {
    if (child.matches(nextSelector)) {
      nextElements.push(child);
    }

    nextElements = nextElements.concat(getNextElements(child, nextSelector));
  });

  return nextElements;
}

const findElement = (selectors, baseElement) => {
  var currentElement = baseElement || document.documentElement;

  for (var i = 0; i < selectors.length; i++) {
    currentElement = getNextElement(currentElement, selectors[i]);
    if (!currentElement) {
      break;
    }
  }

  return currentElement;
}

const findElements = (selectors, baseElement) => {
  var currentElements = [baseElement || document.documentElement];

  for (var i = 0; i < selectors.length; i++) {
    var nextElements = [];
    for (var j = 0; j < currentElements.length; j++) {
      nextElements = uniqueConcat(nextElements, getNextElements(currentElements[j], selectors[i]));
    }
    currentElements = nextElements;
  }

  return currentElements;
}

export const querySelector = (query, fromElement) => {
  var selectors = cleanSelectors(isShadow() ? query.split(' ') : [query]);

  return findElement(selectors, fromElement);
}

export const querySelectorAll = (query, fromElement) => {
  var selectors = cleanSelectors(isShadow() ? query.split(' ') : [query]);

  return findElements(selectors, fromElement);
}

export default {
  querySelector,
  querySelectorAll
};
