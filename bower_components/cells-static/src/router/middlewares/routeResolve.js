import transitionPath from 'router5.transition-path';
import { HOOKS } from '../constants';
import HooksUtils from '../lib/hooks';

const { ROUTER, ROUTE } = HOOKS;
const { getHook, triggerHook } = HooksUtils;

const routeResolve = (hooks) => (router, dependencies) => (toState, fromState, done) => {
  const { routes } = dependencies;
  const toActivatePath = (transitionPath(toState, fromState) || {}).toActivate;
  const onResolveHandlers = toActivatePath.map(segment => routes.find(r => r.name === segment)[ROUTE.onResolve]).filter(Boolean);

  if (hooks.hasOwnProperty(ROUTER.onTransitionResolve)) {
    onResolveHandlers.push(hooks[ROUTER.onTransitionResolve]);
  }

  if (onResolveHandlers && onResolveHandlers.length > 0) {
    return Promise.all(onResolveHandlers.map(onResolvePromiseHandler => onResolvePromiseHandler.call(null, toState, fromState, done))).then(response => {
        console.log('evaluando datos...', response);
        const data = response.reduce((accData, rData) => Object.assign(accData, rData), {});

        return { ...toState, data };
    });
  }

  return true;
};

export default routeResolve;
